/**cria o objeto de configuracao dos emails */
module.exports.arrayColigadas = [
    {
        nome: "Geral",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/GERAL/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/GERAL/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/GERAL/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/GERAL/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/GERAL/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/GERAL/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/GERAL/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/GERAL/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/GERAL/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "arnaldo.marinho@grupotenco.com.br"
            }, {
                email: "andre.tavares@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "eduardo.gribel@grupotenco.com.br"
            }, {
                email: "leonardo.veloso@grupotenco.com.br"
            }, {
                email: "leonardo.bucsan@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "rafael.ferreira@grupotenco.com.br"
            }, {
                email: "rodrigo.capuruco@grupotenco.com.br"
            }, {
                email: "hugo.ribeiro@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "renner.librelato@grupotenco.com.br"
            }, {
                email: "andre.pasini@grupotenco.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }
        ]
    }, {
        nome: "Fechamento Mensal",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [],        
        hora: [],         
        imagens: [
            {
                nome: "CD.jpg",
                arquivo: "./app/imagens/Fechamento Mensal/CD.jpg",
                cid: "cidCD"
            }, {
                nome: 'legenda.jpg',
                arquivo: './app/imagens/Fechamento Mensal/legenda.jpg',
                cid: 'cidLegenda'
            }, {
                nome: 'ocupacao.jpg',
                arquivo: './app/imagens/Fechamento Mensal/ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/Fechamento Mensal/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'vendas.jpg',
                arquivo: './app/imagens/Fechamento Mensal/vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/Fechamento Mensal/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/Fechamento Mensal/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/Fechamento Mensal/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/Fechamento Mensal/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }
        ]
    }, {
        nome: "AGS",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],        
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/AGS/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/AGS/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/AGS/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/AGS/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/AGS/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/AGS/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/AGS/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/AGS/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/AGS/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "camila.arocha@grupotenco.com.br"
            }, {
                email: "gustavo.gomes@grupotenco.com.br"
            }, {
                email: "boby.perkson@grupotenco.com.br"
            }, {
                email: "luciana.ribeiro@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "ARA",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/ARA/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/ARA/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/ARA/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/ARA/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/ARA/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/ARA/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/ARA/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/ARA/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/ARA/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "vanessa.souto@grupotenco.com.br"
            }, {
                email: "henrique.ramos@grupotenco.com.br"
            }, {
                email: "rafael.ferreira@grupotenco.com.br"
            }, {
                email: "tamara.santos@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "fabio.sirkis@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "BRG",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/BRG/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/BRG/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/BRG/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/BRG/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/BRG/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/BRG/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/BRG/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/BRG/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/BRG/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "lais.brito@grupotenco.com.br"
            }, {
                email: "paulo.neto@grupotenco.com.br"
            }, {
                email: "diogo.linhares@grupotenco.com.br"
            }, {
                email: "emerson.santos@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "CRR",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/CRR/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/CRR/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/CRR/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/CRR/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/CRR/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/CRR/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/CRR/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/CRR/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/CRR/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "mirelly.sousa@grupotenco.com.br"
            }, {
                email: "ramon.lapa@grupotenco.com.br"
            }, {
                email: "albina.gama@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "ataina.ferreira@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "ITQ",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/ITQ/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/ITQ/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/ITQ/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/ITQ/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/ITQ/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/ITQ/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/ITQ/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/ITQ/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/ITQ/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "natalia.garcia@grupotenco.com.br"
            }, {
                email: "marcio.santos@grupotenco.com.br"
            }, {
                email: "alexandre.botelho@grupotenco.com.br"
            }, {
                email: "daniela.guedes@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "JRS",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/JRS/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/JRS/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/JRS/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/JRS/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/JRS/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/JRS/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/JRS/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/JRS/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/JRS/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "thiago.sarmanho@grupotenco.com.br"
            }, {
                email: "eduardo.hirata@grupotenco.com.br"
            }, {
                email: "olimpio.couto@grupotenco.com.br"
            }, {
                email: "viviane.spezzia@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "JUA",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [ 
            {
                nome: "CD.png",
                arquivo: "./app/imagens/JUA/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/JUA/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/JUA/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/JUA/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/JUA/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/JUA/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/JUA/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/JUA/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/JUA/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "filipe.durando@grupotenco.com.br"
            }, {
                email: "joao.monteiro@grupotenco.com.br"
            }, {
                email: "marcelo.escobar@grupotenco.com.br"
            }, {
                email: "edilene.diniz@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "LGS",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/LGS/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/LGS/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/LGS/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/LGS/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/LGS/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/LGS/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/LGS/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/LGS/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/LGS/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "andreia.schmidinger@grupotenco.com.br"
            }, {
                email: "andre.pasini@grupotenco.com.br"
            }, {
                email: "herlington.masson@grupotenco.com.br"
            }, {
                email: "andre.baggio@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "RRM",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/RRM/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/RRM/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/RRM/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/RRM/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/RRM/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/RRM/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/RRM/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/RRM/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/RRM/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "paula.mello@grupotenco.com.br"
            }, {
                email: "roberto.machado@grupotenco.com.br"
            }, {
                email: "leandro.lourenco@grupotenco.com.br"
            }, {
                email: "theo.machado@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "VCF",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/VCF/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/VCF/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/VCF/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/VCF/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/VCF/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/VCF/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/VCF/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/VCF/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/VCF/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "mariana.zonta@grupotenco.com.br"
            }, {
                email: "gleiser.guimaraes@grupotenco.com.br"
            }, {
                email: "ataina.ferreira@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ferreira@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }, {
        nome: "VVS",
        html: {
            path: "./app/corpo_email/dashboard_gerencial.ejs"
        },
        dia: [1,2,3,4,5],         
        hora: ['11:30'],         
        imagens: [
            {
                nome: "CD.png",
                arquivo: "./app/imagens/VVS/CD.png",
                cid: "cidCD"
            }, {
                nome: 'Legenda.png',
                arquivo: './app/imagens/VVS/Legenda.png',
                cid: 'cidLegenda'
            }, {
                nome: 'Ocupacao.jpg',
                arquivo: './app/imagens/VVS/Ocupacao.jpg',
                cid: 'cidOcupacao'
            }, {
                nome: 'Inadimplencia.jpg',
                arquivo: './app/imagens/VVS/Inadimplencia.jpg',
                cid: 'cidInadimplencia'
            }, {
                nome: 'Vendas.jpg',
                arquivo: './app/imagens/VVS/Vendas.jpg',
                cid: 'cidVendas'
            }, {
                nome: 'esp.png',
                arquivo: './app/imagens/VVS/esp.png',
                cid: 'cidEsp'
            },  {
                nome: 'estacionamentoreceita.jpg',
                arquivo: './app/imagens/VVS/estacionamentoreceita.jpg',
                cid: 'cidEstacionamentoReceita'
            },  {
                nome: 'estacionamentofluxo.jpg',
                arquivo: './app/imagens/VVS/estacionamentofluxo.jpg',
                cid: 'cidEstacionamentoFluxo'
            }, {
                nome: 'fluxo.jpg',
                arquivo: './app/imagens/VVS/fluxo.jpg',
                cid: 'cidFluxo'
            }
        ],
        contatos: [
            {
                email: "gabriel@vp6.com.br"
            }, {
                email: "igor.flori@grupotenco.com.br"
            }, {
                email: "bruna.marcon@grupotenco.com.br"
            }, {
                email: "caroline.ferreira@grupotenco.com.br"
            }, {
                email: "renato.goncalves@grupotenco.com.br"
            }, {
                email: "patricia.goncalves@grupotenco.com.br"
            }, {
                email: "fabricio.amaral@grupotenco.com.br"
            }, {
                email: "rafael.ril@grupotenco.com.br"
            }, {
                email: "daniella.camargos@grupotenco.com.br"
            }, {
                email: "stefano.mattioli@grupotenco.com.br"
            }, {
                email: "renato.castro@grupotenco.com.br"
            }, {
                email: "milton.sacramento@grupotenco.com.br"
            }
        ]
    }
];