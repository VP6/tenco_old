let arquivo = require('fs');

/**Funcao para retornar o dia da semana */
module.exports.diaSemana = function(){
   
    let data = new Date;
    let diaSemana = data.getDay();
    return diaSemana;
}

/**Funcao para retornar a hora e minuto atual*/
module.exports.horaMinutoAtual = function(){
   
    let data = new Date;
    let hora;

    if (data.getHours() < 10) 
        if(data.getMinutes() >= 10)
            hora = '0' + data.getHours() + ':' + data.getMinutes();
        else
            hora = '0' + data.getHours() + ':0' + data.getMinutes();
    else
        if(data.getMinutes() >= 10)
            hora = data.getHours() + ':' + data.getMinutes();
        else
            hora = data.getHours() + ':0' + data.getMinutes();
    return hora;
}

/**Funcao para retornar a data no formato brasileiro*/
module.exports.dataBrasileira = function(){
    
    let data = new Date;
    let dia,
        mes,
        ano;
    //formata o dias
    dia = data.getDate();
    if (dia< 10) {
        dia  = "0" + dia;
    }
    //formata o mes
    mes = data.getMonth() + 1;
    if (mes < 10) {
        mes  = "0" + mes;
    }
    //formata o ano
    ano = data.getFullYear();
    return dia + '/' + mes + '/' + ano;
}

/**funcao que verifica a existencia de um arquivo. retona true se existir e false se nao existir */
module.exports.existeArquivo = function(caminhoArquivo){
    if((caminhoArquivo == undefined) || (caminhoArquivo == ''))
        return false;
    //se o arquivo foi passado verifica se ele existe
    if (!arquivo.existsSync(caminhoArquivo))
        return false;
    return true;
}