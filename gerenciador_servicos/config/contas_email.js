let nodemailer = require('nodemailer');

/**conta default de envio de email */
module.exports.contaDefault = function(){

    return nodemailer.createTransport({
        pool: true,
        host: 'smtp.office365.com',
        port: 587,
        secure: false,//para conexoes SSL
        requiresAuth: true,
        auth: {
            user: 'nao-responda-aura@grupotenco.com.br',
            pass: 'tenco2120'
        }
    });
};

/**conta de teste para envio de email */
module.exports.contaTeste = function(){

    return nodemailer.createTransport({
        pool: true,
        host: 'smtp.task.com.br',
        port: 587,
        secure: false,//para conexoes SSL
        requiresAuth: false,
        auth: {
            user: 'felipe@infosistemas.com.br',
            pass: 'felinfo'
        }
    });
};

module.exports.enviaEmail = function(conta, dadosEmail, callback){
    conta.sendMail(dadosEmail, (error) => {
        callback(error, null);           
    });
};