let rotinas = require('./../../config/rotinas');
let coligadas = require('./../../config/coligadas').arrayColigadas;
let email = require('./../../config/contas_email');
let conta = require('./../../config/contas_email').contaDefault();

module.exports.enviarEmail = function(){
    
    let contatos;
    let anexos,
        anexo,
        anexosOK,
        achouHora, 
        achouDia;
    let promisses = [];

    let horaAtual = rotinas.horaMinutoAtual();
    let diaSemana = rotinas.diaSemana();

    //percorre as coligadas para enviar o email
    coligadas.forEach(function(coligada){
        
        let achouDia = false;
        let achouHora = false;
        //verifica se a hora atual é a de envio
        for(let x = 0; x < coligada.hora.length; x++){
            if(coligada.hora[x] == horaAtual)
                achouHora = true;
        }

        //verifica se o dia da semana é o do envio
        for(let x = 0; x < coligada.dia.length; x++){
            if(coligada.dia[x] == diaSemana)
                achouDia = true;
        }
        if((achouDia) && (achouHora)){
            //monta a lista de contatos em copia oculta
            contatos = "";
            for(let x = 1; x < coligada.contatos.length; x++){
                if(((x+1) >= coligada.contatos.length) && (contatos == "")) 
                    contatos += coligada.contatos[x].email;
                else
                    contatos += coligada.contatos[x].email + "; ";
            }

            //monta a lista de anexos e verifica a existencia do mesmo
            anexos = [];
            anexosOK = true;
            for(let x = 0; x < coligada.imagens.length; x++){
                anexo = {};
                //verifica se o anexo existe 
                if(!rotinas.existeArquivo(coligada.imagens[x].arquivo))
                    anexosOK = false;
                anexo.filename = coligada.imagens[x].nome;
                anexo.path = coligada.imagens[x].arquivo;
                anexo.cid = coligada.imagens[x].cid;
                anexos.push(anexo);
            }; 

            //se der erro em algum anexo nao envia o email
            if(!anexosOK)
                console.log('Atenção! Email não enviado. Arquivos faltantes para o envio de email.')
            else {
                let dadosEmail = {
                    from: 'Não-Responda <nao-responda-aura@grupotenco.com.br>',
                    to: coligada.contatos[0].email,
                    bcc: contatos,
                    subject: "Dashboard Gerencial - " + coligada.nome + " - " + rotinas.dataBrasileira(),
                    html: coligada.html,
                    attachments: anexos
                };
                email.enviaEmail(conta, dadosEmail, function(erro, success){
                    if(erro) {
                        console.log("Não enviado.");
                    }
                    console.log("Email enviado as " + rotinas.horaMinutoAtual());
                });
            } 
        }
    });
}