const lojaRepasse = require('../models/loja_repasse');
const resposta = require('../../config/rotinas');


module.exports.get = async function (req, res) {
    let retorno = await lojaRepasse.get();
    resposta.montaRetorno(retorno, req, res);
}

module.exports.getColigada = async function (req, res) {
    let retorno = await lojaRepasse.getColigada();
    resposta.montaRetorno(retorno, req, res);
}


module.exports.getFilial = async function (coligada, req, res) {
    let retorno = await lojaRepasse.getFilial(coligada);
    resposta.montaRetorno(retorno, req, res);
}


module.exports.getContrato = async function (filial, req, res) {
    let retorno = await lojaRepasse.getContrato(filial);
    resposta.montaRetorno(retorno, req, res);
}

module.exports.getNomeFantasia = async function (contrato, req, res) {
    let retorno = await lojaRepasse.getNomeFantasia(contrato);
    resposta.montaRetorno(retorno, req, res);
}

module.exports.getTipoLoja = async function (contrato, req, res) {
    let retorno = await lojaRepasse.getTipoLoja(contrato);
    resposta.montaRetorno(retorno, req, res);
}