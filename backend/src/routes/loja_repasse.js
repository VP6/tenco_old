const rotas = require('../../config/routes');
const lojaRepasse = require('../controllers/loja_repasse');

module.exports = function () {



    rotas.get('/lojaRepasse/', function (req, res) {
        lojaRepasse.get(req, res);
    });


    rotas.get('/lojaRepasse/coligada/', function (req, res) {
        lojaRepasse.getColigada(req, res);
    });


    rotas.get('/lojaRepasse/filial/listar/:coligada', function (req, res) {
        lojaRepasse.getFilial(req.params.coligada, req, res);
    });

    rotas.get('/lojaRepasse/contrato/listar/:coligada', function (req, res) {
        lojaRepasse.getContrato(req.params.coligada, req, res);
    });
    rotas.get('/lojaRepasse/nomeFantasia/listar/:contrato', function (req, res) {
        lojaRepasse.getNomeFantasia(req.params.contrato, req, res);
    });
    rotas.get('/lojaRepasse/tipoLoja/abl/listar/:contrato', function (req, res) {
        lojaRepasse.getTipoLoja(req.params.contrato, req, res);
    });
}