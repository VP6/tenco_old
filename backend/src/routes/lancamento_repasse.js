const rotas = require('../../config/routes');
const lancamentoRepasse = require('../controllers/lancamento_repasse');

module.exports = function(){

     rotas.get('/lancamentoRepasse/:id', function(req, res){
        lancamentoRepasse.get(req.params.id, req, res);
     });
 
     rotas.get('/lancamentoRepasse', function(req, res){
        lancamentoRepasse.get(null, req, res);
     });
 
     rotas.post('/lancamentoRepasse', function(req, res){
        lancamentoRepasse.post(req.body, req, res);
     });
 
     rotas.put('/lancamentoRepasse', function(req, res){
        lancamentoRepasse.put(req.body, req, res);
     });
 
     rotas.delete('/lancamentoRepasse/:id', function(req, res){
        lancamentoRepasse.delete(req.params.id, req, res);
     });   
}