const configDB = require('../../config/config_DB');

module.exports.get = async function () {

  
    let script = `SELECT *
                  FROM LojasRepasse (NOLOCK)`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.getColigada = async function () {

    //monta a consulta
    let script = `SELECT DISTINCT COLIGADA
                        FROM LojasRepasse
                        ORDER BY COLIGADA`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.getFilial = async function (coligada) {

    //monta a consulta
    let script = `SELECT DISTINCT IDFILIAL
                        FROM LojasRepasse
                        WHERE COLIGADA LIKE '${coligada}'
                        ORDER BY IDFILIAL`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.getContrato = async function (coligada) {

    //monta a consulta
    let script = `SELECT DISTINCT CONTRATO
                        FROM LojasRepasse
                        WHERE COLIGADA LIKE '${coligada}'
                        ORDER BY CONTRATO`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.getNomeFantasia = async function (contrato) {

    //monta a consulta
    let script = `SELECT DISTINCT NOMEFANTASIA
                        FROM LojasRepasse
                        WHERE CONTRATO = '${contrato}'
                        ORDER BY NOMEFANTASIA`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};
module.exports.getTipoLoja = async function (contrato) {

    //monta a consulta
    let script = `SELECT DISTINCT TIPOLOJA,ABL
                        FROM LojasRepasse
                        WHERE CONTRATO = '${contrato}'
                        ORDER BY TIPOLOJA`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};