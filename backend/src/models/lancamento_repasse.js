const configDB = require('../../config/config_DB');

module.exports.get = async function (id) {

    let parametro = 'WHERE CODIGOREPASSE = ';

    // prepara  o parametro
    if (id === null)
        parametro = '';
    else
        parametro += id;

    //monta a consulta
    let script = `SELECT * FROM REPASSES ${parametro}`;


    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function (lancamentoRepasse) {

    let campos = "";
    let valores = "";

    for (key in lancamentoRepasse) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof lancamentoRepasse[key] !== 'object') {
            if (key !== 'CODIGOREPASSE') {
                if (campos === "") {
                    campos += key;
                    valores += "'" + lancamentoRepasse[key] + "'";
                } else {
                    campos += "," + key;
                    valores += ",'" + lancamentoRepasse[key] + "'";
                }
            }
        }
    };

    let script = `INSERT INTO REPASSES (${campos})
                        VALUES (${valores}) SELECT SCOPE_IDENTITY() AS id`;


    console.log(script);                    
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function (lancamentoRepasse) {

    let valores = "";

    for (key in lancamentoRepasse) { // obtém as chaves do objeto
        // se o valor for diferente de objeto (caso events)
        if (typeof lancamentoRepasse[key] !== 'object') {
            if (key !== 'CODIGOREPASSE') {
                if (valores === "") {
                    valores += key + " ='" + lancamentoRepasse[key] + "'";
                } else {
                    valores += "," + key + " ='" + lancamentoRepasse[key] + "'";
                }
            }
        }
    };

    let script = `UPDATE REPASSES SET ${valores}
                        WHERE CODIGOREPASSE = ${lancamentoRepasse.CODIGOREPASSE}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function (id) {

    //monta a consulta
    let script = `DELETE FROM REPASSES WHERE CODIGOREPASSE = ${id}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};