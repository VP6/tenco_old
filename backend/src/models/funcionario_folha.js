const configDB = require('../../config/config_DB');

module.exports.get = async function(){
    
    //monta a consulta
    let script = `SELECT bFolha.Nome 
                        ,bFolha.CPF 
                            ,CONVERT(VARCHAR(10),bFolha.DataAdmissao,103) AS DataAdmissao 
                            ,CONVERT(VARCHAR(10),NovosLancamentos.DataDemissao,103) AS DataDemissao 
                            ,CASE WHEN NovosLancamentos.Coligada IS NOT NULL THEN NovosLancamentos.Coligada 
                                    ELSE bFolha.cColigada 
                                END AS Sigla 
                            ,CASE WHEN NovosLancamentos.Matricula IS NOT NULL THEN NovosLancamentos.Matricula 
                                ELSE bFolha.Matricula 
                            END AS Matricula 
                            ,CASE WHEN NovosLancamentos.Salario IS NOT NULL THEN NovosLancamentos.Salario 
                                ELSE bFolha.Salario 
                            END AS Salario 
                            ,UPPER(CASE WHEN NovosLancamentos.CCusto IS NOT NULL THEN NovosLancamentos.CCusto  
                                ELSE bFolha.CentroDeResultadoNome 
                            END) AS CentroCusto 
                            ,UPPER(CASE WHEN NovosLancamentos.Diretoria IS NOT NULL THEN NovosLancamentos.Diretoria 
                                ELSE bFolha.cDiretoria 
                            END) AS Diretoria 
                            ,UPPER(CASE WHEN NovosLancamentos.Cargo IS NOT NULL THEN NovosLancamentos.Cargo 
                                ELSE bFolha.CargoNome 
                            END) AS Cargo 
                            ,UPPER(CASE WHEN NovosLancamentos.Status IS NOT NULL THEN NovosLancamentos.Status 
                                ELSE bFolha.SituacaoDoEmpregadoStatus 
                            END) AS Status 
                    FROM bFolha 
                    LEFT JOIN COLIGADA (NOLOCK) ON bFolha.CNPJ = COLIGADA.CNPJ COLLATE SQL_Latin1_General_CP1_CI_AS 
                    LEFT JOIN OrcPessoalFunc (NOLOCK) AS NovosLancamentos ON bFolha.CPF = NovosLancamentos.CPF 
                    WHERE (COLIGADA.CODCOLIGADA IS NOT NULL) 
                    AND (bFolha.DataDeDesligamento IS NULL) 
                    UNION 
                    SELECT OrcPessoalFunc.Nome 
                    ,OrcPessoalFunc.CPF 
                            ,CONVERT(VARCHAR(10),OrcPessoalFunc.DataAdmissao,103) AS DataAdmissao 
                            ,'' AS DataDemissao 
                            ,OrcPessoalFunc.Coligada AS Sigla 
                            ,OrcPessoalFunc.Matricula 
                            ,OrcPessoalFunc.Salario 
                            ,OrcPessoalFunc.CCusto AS CentroCusto 
                    ,OrcPessoalFunc.Diretoria 
                    ,OrcPessoalFunc.Cargo 
                    ,OrcPessoalFunc.Status 
                    FROM OrcPessoalFunc 
                    WHERE (OrcPessoalFunc.Nome = 'NOVO COLABORADOR') 
                    ORDER BY Sigla, Nome`;
                  
    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.post = async function(funcionario){
    
    let script = `INSERT INTO OrcPessoalFunc 
                    (Nome, Coligada, Diretoria, CCusto, Cargo, Status, Salario, DataAdmissao, CPF) 
                    VALUES 
                    ('${funcionario.Nome}','${funcionario.Coligada}','${funcionario.Diretoria}',' 
                      ${funcionario.CentroCusto}','${funcionario.Cargo}','${funcionario.Status}',
                      ${funcionario.Salario},'${funcionario.DataAdmissao}','${funcionario.CPF}')`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.put = async function(funcionario){
    
    //prepara os parametros
    let parametros = "";
    parametros += "(";
    parametros += "'" + funcionario.Nome + "',";
    parametros += "'" + funcionario.Coligada + "',";
    parametros += "'" + funcionario.Diretoria + "',";
    parametros += "'" + funcionario.CentroCusto + "',";
    parametros += "'" + funcionario.Cargo + "',";
    parametros += "'" + funcionario.Status + "',";
    parametros += "'" + funcionario.CPF + "',";
    parametros += "'" + funcionario.Matricula + "',";
    parametros += "" + funcionario.Salario + ",";
    parametros += "'" + funcionario.DataAdmissao + "',";
    if(funcionario.DataDemissao != ""){
        parametros += "'" + funcionario.DataDemissao + "',";
    }
    else {
        parametros += "NULL,";
    }
    if(funcionario.DataSalario != ""){
        parametros += "'" + funcionario.DataSalario + "')";
    }
    else {
        parametros += "NULL)";
    }
    //monta o script
    let script = `INSERT INTO OrcPessoalFunc 
                 (Nome, Coligada, Diretoria, CCusto, Cargo, Status, CPF, Matricula, Salario, 
                 DataAdmissao, DataDemissao, DataSalario)
                 VALUES ${parametros}`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};

module.exports.delete = async function(funcionario){

    //monta a consulta
    let script = `DELETE OrcPessoalFunc 
                  WHERE CPF = '${funcionario.CPF}'`;

    let resultado = await configDB.executaScriptSQL(script);
    return resultado;
};