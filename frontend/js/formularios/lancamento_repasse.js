let registros = [];
let contrato = [];
let nomeFantasia;
let tipoLoja;
let abl;
let contratoloja;



function controlaPanel(exibeFormulario) {
    if (exibeFormulario) {
        $('#formulario').show();
        $('#lista').hide();
        $('#filtros').hide();
    } else {
        $('#formulario').hide();
        $('#lista').show();
        $('#filtros').show();
    }
}

function novoRegistro() {
    controlaPanel(true);
    document.getElementById('formulario').reset();
}

function cancelarRegistro() {
    controlaPanel(false);
}


$(window).on("load", function () {
    carregaColigada();
    carregaLojaRepasse();


});

$('#selColigada').on('change', function () {
    if (this.value != '') {
        carregaFilial(this.value);
        carregaContrato(this.value);
    }
});

$('#txtContrato').on('change', function () {
    if (this.value == '') {
        limparCampos();
    }

    if (this.value != '') {
        for (let x in contrato) {
            if (this.value == contrato[x].CONTRATO) {
                contratoloja = contrato[x].CONTRATO;
                limparCampos();
                carregaNomeFantasia(this.value);
                carregaTipoLoja(this.value);
                clearContrato = contratoloja;

            }

        }
        if (this.value != contratoloja) {
            $('#modal_contrato').modal('show');
            let mensagem = document.getElementById('mensagem_modal_contrato');
            mensagem.innerHTML = 'Contrato inexistente!';
            this.value = innerHTML = '';
        }




    }
});


function limparCampos() {
    document.getElementById('lblNomeFantasia').innerHTML = '';
    document.getElementById('lblTipoLoja').innerHTML = '';
    document.getElementById('lblAbl').innerHTML = '';

}

function salvarRegistro() {

    let codigo = document.getElementById('txtCodigo').value;
    let idFilial = document.getElementById('selFilial').value;
    let coligada = document.getElementById('selColigada').value;
    let contrato = document.getElementById('txtContrato').value;
    let data = formataData(document.getElementById('txtDataEncerramento').value);
    let dataInicio = formataData(document.getElementById('txtDataInicio').value);
    let status = innerHTML = '';

    let registro = {};
    if (codigo != '') {
        registro.CODIGOREPASSE = codigo;
    }
    registro.COLIGADA = coligada;
    registro.IDFILIAL = idFilial;
    registro.CONTRATO = contrato;
    registro.NOMEFANTASIA = nomeFantasia;
    registro.TIPOLOJA = tipoLoja;
    registro.ABL = abl;





    let dados = JSON.stringify(registro);
    debugger
    if (codigo == '')
        for (let index = 0; index <= 1; index++) {

            if (index == 0) {
                status = 'Ativo';
                registro.STATUS = status;
                registro.DATAINICIO = dataInicio;
                gravarBD(JSON.stringify(registro));
            } else {
                status = 'Rescindido';
                registro.STATUS = status;
                registro.DATAFECHAMENTO = data;
                delete registro.DATAINICIO;
                gravarBD(JSON.stringify(registro));
            }

        }

    else
        alterarBD(JSON.stringify(registro)) ;

}


function exibirConfirmarExcluir(indice) {
    let mensagem = document.getElementById('mensagem_modal_confirmar');
    mensagem.innerHTML = 'Atenção! Confirma a exclusão do registro?';
    //captura o button de confirmar do modal_confirmar
    let btnModalConfirmar = document.getElementById('btnModalConfirmar');
    //cria o evento onclick
    let onClick = document.createAttribute('onclick');
    //define o evento onclick
    onClick.value = 'excluirRegistro(' + indice + ')';
    //atribui ao elemento html
    btnModalConfirmar.attributes.setNamedItem(onClick);
    $('#modal_confirmar').modal('show');
}

function excluirRegistro(indice) {
    $('#modal_confirmar').modal('hide');
    let codigo = registros[indice].CODIGOREPASSE;
    deletarBD(codigo);
    //verifica se o indice a ser deletado é o ultimo do array de registros
    if (indice === registros.length - 1) {
        registros.pop();
    } else if (indice === 0) { //verifica se o indice a ser deletado é o primeiro do array de registros
        registros.shift();
    } else {
        let auxInicio = registros.slice(0, indice);
        let auxFim = registros.slice(indice + 1);
        registros = auxInicio.concat(auxFim);
    }
    preencheTable();
}



function preencheTable() {
    let tabela = document.getElementById('lista_corpo');
    tabela.innerHTML = '';
    for (let i in registros) {
        tabela.innerHTML +=
            `
        <tr>
            <td>${registros[i].COLIGADA}</td>
            <td>${registros[i].IDFILIAL}</td>
            <td>${registros[i].CONTRATO}</td>
            <td>${registros[i].NOMEFANTASIA}</td>
            <td>${registros[i].TIPOLOJA}</td>
            <td>${registros[i].ABL}</td>
            <td>${formataDataBrasileira(registros[i].DATAINICIO)}</td>
            <td>${formataDataBrasileira(registros[i].DATAFECHAMENTO)}</td>
            <td>${registros[i].STATUS}</td>
            <td style="white-space: nowrap">
                    <button class="btn btn-danger btn-xs glyphicon glyphicon-trash" title="Excluir" onclick="exibirConfirmarExcluir(${i})"></button>
            </td>
        </tr>
        `
    }
}

//funcao para gravar um novo registro no bd
function gravarBD(dados) {
    console.log('dados', dados);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_SERVICO + "/lancamentoRepasse", false); //realiza uma chamada sincrona para receber o id gerado
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
    if (xhr.status === 200) {
        var data = $.parseJSON(xhr.responseText).result;
        return data[0].id;
    }
};

//funcao para alterar um registro no bd
function alterarBD(dados) {
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", BASE_URL_SERVICO + "/lancamentoRepasse", false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(dados);
}

//funcao para deletar um registro no bd
function deletarBD(id) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", BASE_URL_SERVICO + "/lancamentoRepasse/" + id);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
}

function carregaColigada() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lojaRepasse/coligada/');
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selColigada').innerHTML = '';
            $('#selColigada').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selColigada').append(`<option value="${data[x].COLIGADA}">${data[x].COLIGADA}</option>`);

            }
        }
    }
    xhr.send();
}


function carregaContrato(coligada) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lojaRepasse/contrato/listar/' + coligada, false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            contrato = data;
        }

    }
    xhr.send();
}

function carregaNomeFantasia(contrato) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lojaRepasse/nomeFantasia/listar/' + contrato, false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            $('#lblNomeFantasia').append(`<label></label>`);
            for (let x in data) {
                $('#lblNomeFantasia').append(`<label>${data[x].NOMEFANTASIA}</label>`);
                nomeFantasia = data[x].NOMEFANTASIA;
                let contratoAnterior = contrato;
                $('#txtContrato').on('change', function () {
                    if (contratoAnterior != contrato) {
                        $('#lblNomeFantasia').append(`<label></label>`);
                    }

                });

            }
        }

    }
    xhr.send();
}

function carregaTipoLoja(contrato) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lojaRepasse/tipoLoja/abl/listar/' + contrato, false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            $('#lblTipoLoja').append(`<label></label>`);
            $('#lblAbl').append(`<label></label>`);
            for (let x in data) {
                $('#lblTipoLoja').append(`<label>${data[x].TIPOLOJA}</label>`);
                $('#lblAbl').append(`<label>${data[x].ABL}</label>`);
                tipoLoja = data[x].TIPOLOJA;
                abl = data[x].ABL;
            }
        }

    }
    xhr.send();
}


function carregaFilial(coligada) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lojaRepasse/filial/listar/' + coligada, false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            document.getElementById('selFilial').innerHTML = '';
            $('#selFilial').append(`<option value="0"></option>`);
            for (let x in data) {
                $('#selFilial').append(`<option value="${data[x].IDFILIAL}">${data[x].IDFILIAL}</option>`);
            }
        }
    }
    xhr.send();
}
//funcao para carregar os registros do bd


//funcao para carregar os registros do bd
function carregaLojaRepasse() {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', BASE_URL_SERVICO + '/lancamentoRepasse', false);
    xhr.onload = function () {
        if (xhr.status == 200) {
            let data = $.parseJSON(xhr.responseText).result;
            registros = data;
            preencheTable();
        }
    }
    xhr.send();
}






