<?php
    require_once("layout/cabecalho_layout.php");
?>
    <div class="container">

        <!-- formulario filtro -->
        <div class="panel panel-primary" id="filtros">
            <div class="panel-heading">Filtros</div>
            <div class="panel-body">
                <form class="form-inline">
                    <label class="control-label" for="txtBuscar">Pesquise na tabela por:</label>
                    <input type="text" id="txtBuscar" placeholder="Buscar por" class="form-control input-sm">&nbsp;
                    <button type="button" class="form-control btn btn-success btn-sm" onclick="novoRegistro()">Inserir</button>
                </form>
            </div>
        </div>

        <!-- tabela dos registros -->
        <div class="panel panel-primary" id="lista">
            <div class="panel-heading">Listas de Repasse</div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="lista_registros">
                        <thead>
                            <tr>
                                <th>Coligada</th>
                                <th>Id Filial</th>
                                <th>Contrato</th>
                                <th>Nome Fantasia</th>
                                <th>Tipo Loja</th>
                                <th>ABL</th>
                                <th>Data Início</th>
                                <th>Data Encerramento</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="lista_corpo">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Formulario para manipulacao dos registros -->
        <form onsubmit="salvarRegistro()" id="formulario" method="post" hidden>
            <div class="panel panel-primary">
                <div class="panel-heading">Adicionar Repasse</div>
                <div class="panel-body">
                    <div class="form-group row">
                        <input type="text" name="txtCodigo" id="txtCodigo" hidden>
                        <div class="form-group-sm col-sm-8 col-md-2">
                            <label class="control-label" for="selColigada">Coligada</label>
                            <select id="selColigada" class="form-control" required>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group-sm col-sm-8 col-md-2">
                            <label class="control-label" for="selFilial">ID Filial</label>
                            <select id="selFilial" class="form-control" required>
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="form-group-sm col-sm-8 col-md-2">
                            <label class="control-label" for="txtContrato">Contrato</label>
                            <input type="text" id="txtContrato" class="form-control" required>
                        </div>
                        <div class="form-group-sm col-sm-8 col-md-3">
                            <label class="control-label" for="txtDataInicio">Data de Início</label>
                            <input type="text" id="txtDataInicio" class="form-control data" required>
                        </div>
                        <div class="form-group-sm col-sm-8 col-md-3">
                            <label class="control-label" for="txtDataEncerramento">Data Encerramento</label>
                            <input type="text" id="txtDataEncerramento" class="form-control data" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-group-sm col-sm-8 col-md-12">
                            <div class="jumbotron">
                                <div class="container">
                                    <div class="form-group-sm col-sm-8 col-md-4">
                                        <div class="fixed-bottom">
                                            <label class="cabecalho">Nome Fantasia</label>
                                            <br>
                                        </div>
                                        <label id='lblNomeFantasia'></label>
                                    </div>
                                    <div class="form-group-sm col-sm-8 col-md-4">
                                        <label>Tipo Loja</label>
                                        <br>
                                        <label id='lblTipoLoja'></label>
                                    </div>
                                    <div class="form-group-sm col-sm-8 col-md-4">
                                        <label>ABL(m²)</label>
                                        <br>
                                        <label id='lblAbl'></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                    <button type="button" class="btn btn-default btn-sm" onclick="cancelarRegistro()">Cancelar</button>
                </div>
            </div>
        </form>

        <!-- Formulario modal de confirmacao-->
        <div class="modal fade" id="modal_confirmar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Confirmar</h5>
                    </div>
                    <div class="modal-body">
                        <p id="mensagem_modal_confirmar"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="btnModalConfirmar">Sim</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Formulario modal de confirmacao-->
        <div class="modal fade" id="modal_contrato" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="modal-title">Confirmar</h5>
                    </div>
                    <div class="modal-body">
                        <p id="mensagem_modal_contrato"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <script src="js/mask.js"></script>
    <script src="js/mask_money.js"></script>
    <script src="js/config.js"></script>
    <script src="js/formularios/lancamento_repasse.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>


    <?php
    require_once("layout/rodape_layout.php");
?>